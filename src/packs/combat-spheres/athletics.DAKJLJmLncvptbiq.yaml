_id: DAKJLJmLncvptbiq
_key: "!journal!DAKJLJmLncvptbiq"
flags:
  pf1spheres:
    sphere: athletics
folder: null
name: Athletics
pages:
  - _id: HU03DEUruhT9DyUR
    _key: "!journal.pages!DAKJLJmLncvptbiq.HU03DEUruhT9DyUR"
    flags: {}
    image: {}
    name: Athletics
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/athletics.webp"
        style="float:right;border:none;outline:none" />When you gain the
        Athletics sphere, you gain one of the following packages with its
        included abilities. You may take the Expanded Training talent to gain
        additional packages.

        <p>Each package has an associated movement mode and an associated skill.
        Some abilities require you to be using an associated movement mode to
        function. When you gain a package, you gain 5 ranks in its associated
        skill, plus 5 ranks per additional talent spent in the Athletics sphere
        (maximum ranks equal to your total Hit Dice). If you already have ranks
        in the associated skill you may immediately retrain them, but you do not
        get to retrain when only temporarily gaining talents, such as through
        the armiger’s customized weapons class feature.</p>

        <p>If you possess both the (leap) and (run) packages, you gain a
        competence bonus on Acrobatics checks equal to half your base attack
        bonus instead of retraining the ranks a second time.</p>

        <p>In addition, you gain the following ability:</p>

        <h1 id="toc0"><span><span style="color:#993300">Coordinated
        Movement</span></span></h1>

        <p>Whenever you take the withdraw action, you regain your martial
        focus.</p>

        <hr />

        <h1 id="toc1"><span><span style="color:#993300">Athletics
        Packages</span></span></h1>

        <h4 id="toc2"><span>Climb</span></h4>

        <p>You retain your Dexterity bonus to AC while climbing and may climb at
        half your base speed instead of one-quarter and may move at your full
        speed instead of half speed when taking a -5 penalty. <strong>Associated
        Movement Mode:</strong> Climbing. <strong>Associated Skill:</strong>
        Climb.</p>

        <h4 id="toc3"><span>Fly</span></h4>

        <p>You do not need to make a Fly check to remain flying when moving less
        than half your speed on your turn and count as being one size larger
        when determining the effects of wind on you while flying. This does not
        give you the ability to fly; it simply augments your movement abilities
        if you possess a means of flight. You may gain ranks in the Fly skill
        even if you do not possess a means of flight. <strong>Associated
        Movement Mode:</strong> Flying. <strong>Associated Skill:</strong>
        Fly.</p>

        <h4 id="toc4"><span>Leap</span></h4>

        <p>You may reduce the effective height of any fall by 20 ft. on a
        successful DC 15 Acrobatics check rather than 10. In addition, the fall
        is reduced by an additional 10 ft. for every 10 points this check
        exceeds the target DC. <strong>Associated Movement Mode:</strong>
        Jumping. <strong>Associated Skill:</strong> Acrobatics.</p>

        <h4 id="toc5"><span>Run</span></h4>

        <p>You move five times your normal speed while running if wearing
        medium, light, or no armor and carrying no more than a medium load, or
        four times your speed if wearing heavy armor or carrying a heavy load.
        If you make a jump after a running start (see Acrobatics), you gain a +4
        bonus on your check. You retain your Dexterity bonus to your Armor Class
        while running. <strong>Associated Movement Mode:</strong> Ground.
        <strong>Associated Skill:</strong> Acrobatics. <strong>Associated
        Feat:</strong> Run.</p>

        <h4 id="toc6"><span>Swim</span></h4>

        <p>You may perform the charge and withdraw actions while swimming, and
        can make standard actions and attack actions without decreasing the
        duration for which you can hold your breath. A successful Swim check
        means you may move up to your base land speed as a full-round action (as
        opposed to 1/2 your base land speed) or 1/2 your base land speed as a
        move action (as opposed to 1/4th your base land speed).
        <strong>Associated Movement Mode:</strong> Swimming. <strong>Associated
        Skill:</strong> Swim.</p>

        <hr />

        <h3 id="toc7"><span><span style="color:#993300">Athletic Talent
        Types</span></span></h3>

        <p>Talents possessing a tag matching one of the packages cannot be taken
        unless you possess that package.</p>

        <h4 id="toc8"><span>Motion</span></h4>

        <p>Some talents have the (motion) tag. You cannot apply more than one
        talent with the (motion) tag to a given movement.</p>

        <hr /><br />

        <br /><b>Athletics Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.jnDOsCi3GryaCZ94]{Ace
        Pilot}</li>

        <li>@Compendium[pf1spheres.combat-talents.lyeMc0ZSzF9OVIb0]{Acrocatics}</li>

        <li>@Compendium[pf1spheres.combat-talents.HB3I3DYfW7KDN2HR]{Armored
        Athlete}</li>

        <li>@Compendium[pf1spheres.combat-talents.TrHklajIMMjFIrhs]{Canny
        Compression}</li>

        <li>@Compendium[pf1spheres.combat-talents.8UfDVwhzmlIvAgrE]{Close
        Quarters Training}</li>

        <li>@Compendium[pf1spheres.combat-talents.nKCB9EPBowEc6XWN]{Compact
        Frame}</li>

        <li>@Compendium[pf1spheres.combat-talents.2RjK8HPvGZw1WlIm]{Desperate
        Dive}</li>

        <li>@Compendium[pf1spheres.combat-talents.AmmJrsNGesCDOC7v]{Dungeon
        Blitzer}</li>

        <li>@Compendium[pf1spheres.combat-talents.CXJl9mnlSn2eoSPu]{Elevation
        Mastery}</li>

        <li>@Compendium[pf1spheres.combat-talents.58aQRnluCwdu6Lk9]{Expanded
        Training}</li>

        <li>@Compendium[pf1spheres.combat-talents.llKTYEyBLbhOuOjq]{Lashing
        Serpent}</li>

        <li>@Compendium[pf1spheres.combat-talents.9k8afNFNVfleUgNX]{Mighty
        Conditioning}</li>

        <li>@Compendium[pf1spheres.combat-talents.GBshaatRbE4sW1t2]{Mobile
        Striker}</li>

        <li>@Compendium[pf1spheres.combat-talents.vC8yNvDqt6JOI6GY]{Mobility}</li>

        <li>@Compendium[pf1spheres.combat-talents.E9T6X07s76KfQnLA]{Move
        Aside}</li>

        <li>@Compendium[pf1spheres.combat-talents.yEKGLmXg6y3MVAVE]{Multiple
        Motion}</li>

        <li>@Compendium[pf1spheres.combat-talents.5PF9GxU8QsDhTcNe]{Reactive
        Motion}</li>

        <li>@Compendium[pf1spheres.combat-talents.UZAZmrWDSUyINPhZ]{Reflexive
        Twist}</li>

        <li>@Compendium[pf1spheres.combat-talents.dhnJPEK2o9XqiTt0]{Skillful
        Charge}</li>

        <li>@Compendium[pf1spheres.combat-talents.UHbppd9GWEeFO2Z1]{Slippery
        Weasel’s Tumble}</li>

        <li>@Compendium[pf1spheres.combat-talents.t203ku0YeqtriC8j]{Swift
        Movement}</li>

        <li>@Compendium[pf1spheres.combat-talents.78e9bwXxyN42fdtE]{Whirlwind
        Flip}</li>

        </ul><br /><b>Climb Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.cegjyLhyKyEI3n5a]{Rope
        Swing}</li>

        <li>@Compendium[pf1spheres.combat-talents.PCjITi9YJAMfBNfG]{Scale
        Foe}</li>

        <li>@Compendium[pf1spheres.combat-talents.Fb0qzf3WGWgUDJPF]{Sure
        Grip}</li>

        </ul><br /><b>Fly Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.8tcN3JaoluUWldkf]{Diving
        Strike}</li>

        <li>@Compendium[pf1spheres.combat-talents.hNqY1MUcZYrIsDoa]{Powerful
        Wings}</li>

        <li>@Compendium[pf1spheres.combat-talents.5Rd8oywvreRYnY94]{Sinking
        Blow}</li>

        </ul><br /><b>Leap Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.zpmrFn1GNty3MtDd]{Armored
        Drop}</li>

        <li>@Compendium[pf1spheres.combat-talents.8tcN3JaoluUWldkf]{Diving
        Strike}</li>

        <li>@Compendium[pf1spheres.combat-talents.dy3Qjc0eAPcZvTwm]{Dolphin
        Strike}</li>

        <li>@Compendium[pf1spheres.combat-talents.GfMBa1aO2JrWlvSJ]{Locust
        Pounce}</li>

        <li>@Compendium[pf1spheres.combat-talents.5oj2UpbmK7VlZYuS]{Overhead
        Flip}</li>

        <li>@Compendium[pf1spheres.combat-talents.I8DRefpmhA8sgOBc]{Store
        Momentum}</li>

        <li>@Compendium[pf1spheres.combat-talents.GBgyF8CwhbBEo6WQ]{Unwilling
        Boost}</li>

        <li>@Compendium[pf1spheres.combat-talents.M7AfDfv9A7ohNPOm]{Wall
        Stunt}</li>

        </ul><br /><b>Motion Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.XFqfKQhkmYAKK6fP]{Bewildering
        Blow}</li>

        <li>@Compendium[pf1spheres.combat-talents.7trOQTZsjgD8aByk]{Blustering
        Hustle}</li>

        <li>@Compendium[pf1spheres.combat-talents.X9J7xCg05PgIZtEH]{Deceptive
        Movement}</li>

        <li>@Compendium[pf1spheres.combat-talents.1v9wFJogx2nCqPao]{Dizzying
        Tumble}</li>

        <li>@Compendium[pf1spheres.combat-talents.aGi7vb0nrjikz1Hs]{Knockdown
        Tumble}</li>

        <li>@Compendium[pf1spheres.combat-talents.V7Jd5R8JBNEkSxKO]{Moving
        Target}</li>

        <li>@Compendium[pf1spheres.combat-talents.PCjITi9YJAMfBNfG]{Scale
        Foe}</li>

        <li>@Compendium[pf1spheres.combat-talents.G25nnBnZ5bX6aKsm]{Shoot And
        Scoot}</li>

        <li>@Compendium[pf1spheres.combat-talents.HjYLDN0Q1RyXQenZ]{Sudden
        Flank}</li>

        <li>@Compendium[pf1spheres.combat-talents.RhhfVnctG8VuxYh2]{Wind-Up
        Brace}</li>

        </ul><br /><b>Run Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.chk0Wq3C4e8MKcuT]{Chargethrough}</li>

        <li>@Compendium[pf1spheres.combat-talents.XP8X3XgrUvCmtPgI]{Sharp
        Turn}</li>

        <li>@Compendium[pf1spheres.combat-talents.I8DRefpmhA8sgOBc]{Store
        Momentum}</li>

        <li>@Compendium[pf1spheres.combat-talents.7zsMyQWDfCUIi73K]{Strong
        Lungs}</li>

        <li>@Compendium[pf1spheres.combat-talents.AP8TZlCJwwJUEbWS]{Tumbling
        Recovery}</li>

        <li>@Compendium[pf1spheres.combat-talents.M7AfDfv9A7ohNPOm]{Wall
        Stunt}</li>

        </ul><br /><b>Swim Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.dy3Qjc0eAPcZvTwm]{Dolphin
        Strike}</li>

        <li>@Compendium[pf1spheres.combat-talents.5Rd8oywvreRYnY94]{Sinking
        Blow}</li>

        <li>@Compendium[pf1spheres.combat-talents.7zsMyQWDfCUIi73K]{Strong
        Lungs}</li>

        <li>@Compendium[pf1spheres.combat-talents.UYSQ4mYzeygwg5s9]{Terror
        Below}</li>

        </ul><br /><b>Legendary Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.combat-talents.rnYgorLlPj3py7dH]{Aerial
        Hang}</li>

        <li>@Compendium[pf1spheres.combat-talents.ZtjatBSTuYYKoisx]{Afterimage}</li>

        <li>@Compendium[pf1spheres.combat-talents.0gjFe0hN9vtMqYnw]{Air
        Stunt}</li>

        <li>@Compendium[pf1spheres.combat-talents.jSnNZhvoBwiaTsG5]{Armored
        Apocalypse}</li>

        <li>@Compendium[pf1spheres.combat-talents.oJafbVpooTHXUrQi]{Bomb
        Jump}</li>

        <li>@Compendium[pf1spheres.combat-talents.42vz2JcDxmlvbkRt]{Debris}</li>

        <li>@Compendium[pf1spheres.combat-talents.pbN5XU3ZJEJDQPml]{Dragoon
        Leap}</li>

        <li>@Compendium[pf1spheres.combat-talents.Dsu9d6Nr8sApvwfZ]{Eagle’s
        Path}</li>

        <li>@Compendium[pf1spheres.combat-talents.0dPnre7fi7GbTLz1]{Falling
        Mountain}</li>

        <li>@Compendium[pf1spheres.combat-talents.0HiU4kGny2nCOlII]{Flash
        Step}</li>

        <li>@Compendium[pf1spheres.combat-talents.cISsFRlxc7BgoxJv]{Friction
        Manipulation}</li>

        <li>@Compendium[pf1spheres.combat-talents.EsBSkcq4uC3Ewkjl]{Shark
        Swim}</li>

        <li>@Compendium[pf1spheres.combat-talents.vbprsOqapV4JCyuz]{Slipstream}</li>

        <li>@Compendium[pf1spheres.combat-talents.69wOhGK54HLO4ImZ]{Sparrow’s
        Path}</li>

        <li>@Compendium[pf1spheres.combat-talents.1XC4nbQOvRJhnaaI]{Speed
        Boost}</li>

        <li>@Compendium[pf1spheres.combat-talents.LmpDNQVcahAcCHQc]{Sky Spider’s
        Touch}</li>

        <li>@Compendium[pf1spheres.combat-talents.2akku7uWzxIUSz0x]{Terrain
        Glide}</li>

        <li>@Compendium[pf1spheres.combat-talents.3cgv4WQWa04Uf2NC]{Titan’s
        Fall}</li>

        <li>@Compendium[pf1spheres.combat-talents.mj2g2wwk6PJDcOC4]{Turbo
        Knockdown (Athletics)}</li>

        <li>@Compendium[pf1spheres.combat-talents.z9k7huLAnUjmoUfY]{Turbo Sweep
        (Athletics)}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
