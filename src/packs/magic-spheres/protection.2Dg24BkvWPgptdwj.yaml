_id: 2Dg24BkvWPgptdwj
_key: "!journal!2Dg24BkvWPgptdwj"
flags:
  pf1spheres:
    sphere: protection
folder: null
name: Protection
pages:
  - _id: MD1IfBcSxtwPR1uM
    _key: "!journal.pages!2Dg24BkvWPgptdwj.MD1IfBcSxtwPR1uM"
    flags: {}
    image: {}
    name: Protection
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/protection.webp"
        style="float:right;border:none;outline:none" />You are a user of the
        magics of preservation.

        <h2 id="toc0"><span>Aegis</span></h2>

        <p>As a standard action, you may touch a creature and spend a spell
        point, granting them an aegis for 1 hour per caster level. Unwilling
        targets are allowed a Will save to resist gaining an aegis, and aegis is
        subject to spell resistance.</p>

        <p>Multiple aegises of the same talent can be placed on a single
        creature, and the creature can benefit from them when they are providing
        different effects. This means Armored Magic can give a target both armor
        and shield, and Energy Resistance can grant resistance to multiple
        energy types to the same target.</p>

        <p>You gain the following aegis when you gain the Protection sphere:</p>

        <h4 id="toc1"><span>Deflection</span></h4>

        <p>You grant the target a +1 deflection bonus to AC, +1 per 5 caster
        levels.</p>

        <h2 id="toc2"><span>Ward</span></h2>

        <p>As a standard action, you may create a ward centered on yourself with
        a radius of up to 10 feet + 5 feet per caster level, but can also be
        made so small as to only cover yourself. Wards remain as long as you
        concentrate, but you may spend a spell point to allow them to remain for
        1 round per caster level without concentration. Wards remain in the
        location they were created, even if you move (however, if the ward is
        created entirely on top of a vehicle, it will move with that vehicle).
        If a ward affects targets inside it, its effects are subject to spell
        resistance.</p>

        <p>When you gain the Protection sphere, you gain the following ward:</p>

        <h4 id="toc3"><span>Barrier</span></h4>

        <p>You may create a ward that creates a mostly-transparent wall of force
        at its perimeter. While this barrier does not block line of sight, it
        does block line of effect; thus, while you could still target a creature
        through the barrier (such as for an attack that attempts to break the
        barrier and still deal damage to the creature on the other side), the
        barrier does stop attacks, movement, breath weapons, and any spells or
        sphere effects that rely on line of effect until the barrier is
        destroyed. Ethereal creatures are technically stopped by the barrier,
        but can usually find a way around it (as the barrier does not cut
        through objects, and so usually stops at ground level). Creatures inside
        a space where a barrier is created are shunted to the nearest empty
        space on the outside.</p>

        <p>A barrier has hit points equal to twice your caster level and a Break
        DC of 15 + 1/2 your caster level. A barrier can hold weight, up to 2,400
        lbs. + 250 lbs. per caster level; beyond that, a barrier simply shatters
        as if broken with a Strength check. If a barrier is broken anywhere, the
        entire effect ends.</p>

        <p>If an attack is directed at a target through the barrier, the attack
        first deals its damage to the barrier itself. If this damage is enough
        to destroy the barrier, the attack continues on to its intended target,
        although damage dealt to the barrier is subtracted from any damage done
        to the target or targets. Burst-effect attacks such as splash weapons,
        fireball spells, or others attempting to travel through the barrier
        explode at the barrier’s edge and also must overcome the barrier’s hit
        points to damage targets on the other side. If you maintain your barrier
        through concentration, its hit points are renewed each round on your
        turn.</p>

        <h3 id="toc4"><span>Protection Talent Types</span></h3>

        <p>Certain protection talents grant you additional aegises or wards, and
        are marked (aegis) or (ward) respectively.</p>

        <h4 id="toc5"><span>Succor</span></h4>

        <p>A (succor) talent allows you to create an effect by sacrificing an
        aegis you created. This is an immediate action that can be performed
        using any aegis you created that you have line of effect to. The
        resulting effect occurs to the creature that bore the aegis. These
        talents require line of sight, but otherwise have unlimited range.</p>

        <hr /><br />

        <br /><b>Protection Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.RlWpcih6fceBvhaJ]{Buttressing}</li>

        <li>@Compendium[pf1spheres.magic-talents.M45qbkkn7Nz7ti3g]{Community}</li>

        <li>@Compendium[pf1spheres.magic-talents.NlPAnVwaYAoUiSXd]{Continuous
        Ward}</li>

        <li>@Compendium[pf1spheres.magic-talents.QRNX3QM2Z2CuHrOr]{Distant
        Protection}</li>

        <li>@Compendium[pf1spheres.magic-talents.fxA8pqEk3d2ZYRXG]{Durable
        Barrier}</li>

        <li>@Compendium[pf1spheres.magic-talents.7Eb7OxKxccXG5dmh]{Enduring
        Protection}</li>

        <li>@Compendium[pf1spheres.magic-talents.19uJagVe866DzqWy]{Glyph}</li>

        <li>@Compendium[pf1spheres.magic-talents.2iGjuzmvXCCknPcu]{Greater
        Barrier}</li>

        <li>@Compendium[pf1spheres.magic-talents.TEQ3KC6TuXO9WXke]{Instill
        Aegis}</li>

        <li>@Compendium[pf1spheres.magic-talents.UmTmvWtu8olDJIct]{Lingering
        Succor}</li>

        <li>@Compendium[pf1spheres.magic-talents.6srCCo2nRgtBKxEM]{Mass
        Aegis}</li>

        <li>@Compendium[pf1spheres.magic-talents.pyaYmhPUx2EinHtp]{Reactive
        Barrier}</li>

        <li>@Compendium[pf1spheres.magic-talents.38kAHCg1RcXFE4H2]{Selective
        Barrier}</li>

        <li>@Compendium[pf1spheres.magic-talents.WTA0qomlpPGYfMcy]{Shaped
        Ward}</li>

        <li>@Compendium[pf1spheres.magic-talents.K0yJE3oSwndvhSZ1]{Shared
        Aegis}</li>

        <li>@Compendium[pf1spheres.magic-talents.wb6n8L3DyHq1pmKH]{Status}</li>

        </ul><br /><b>Aegis/Ward Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.LKEsOpBSM86qaH5B]{Ablating}</li>

        <li>@Compendium[pf1spheres.magic-talents.PcnwCB1THnbz4Kgf]{Armored
        Magic}</li>

        <li>@Compendium[pf1spheres.magic-talents.QtxGz4cQxxpGg4nZ]{Breathless}</li>

        <li>@Compendium[pf1spheres.magic-talents.1XbKN0vM9rkMdsz5]{Clarity}</li>

        <li>@Compendium[pf1spheres.magic-talents.76TaHNMQMGNNeoVd]{Deathless}</li>

        <li>@Compendium[pf1spheres.magic-talents.tO5685ph1lFFAWMK]{Destructionless}</li>

        <li>@Compendium[pf1spheres.magic-talents.WKhvWncFJ8WkQ72W]{Energy
        Resistance}</li>

        <li>@Compendium[pf1spheres.magic-talents.AT0UdRTNqiOY7EnK]{Exclusion}</li>

        <li>@Compendium[pf1spheres.magic-talents.0zXQkWsTsEN3aCmd]{Eyeless}</li>

        <li>@Compendium[pf1spheres.magic-talents.UXQWJ4hN1v6uhSHP]{Fateless}</li>

        <li>@Compendium[pf1spheres.magic-talents.eAly59SzX10U1HeX]{Friendship}</li>

        <li>@Compendium[pf1spheres.magic-talents.VBMt70NJnOMc9rJa]{Guardian}</li>

        <li>@Compendium[pf1spheres.magic-talents.p7oTkEgTJgLHvif2]{Impartiality}</li>

        <li>@Compendium[pf1spheres.magic-talents.vKIzKgqF5i7TsG89]{Impedance}</li>

        <li>@Compendium[pf1spheres.magic-talents.vvx5aFbbSNJeayfY]{Inner
        Peace}</li>

        <li>@Compendium[pf1spheres.magic-talents.xP9fblSnobFh9GA9]{Iron
        Shield}</li>

        <li>@Compendium[pf1spheres.magic-talents.yuJuG6crAnAko4Vu]{Logic}</li>

        <li>@Compendium[pf1spheres.magic-talents.q0WpB8qzv6iQEaWW]{Magnetic
        Shield}</li>

        <li>@Compendium[pf1spheres.magic-talents.7fzNd05CXIE3Pqyo]{Mettle}</li>

        <li>@Compendium[pf1spheres.magic-talents.AaU3VTasVx9jRqxK]{Missile
        Shield}</li>

        <li>@Compendium[pf1spheres.magic-talents.Gfo3vcJDKpGAcbBQ]{Mystic
        Shell}</li>

        <li>@Compendium[pf1spheres.magic-talents.9re2sZN7tmp99D4Q]{Obscurity}</li>

        <li>@Compendium[pf1spheres.magic-talents.6rv2hWrtjJ4blCRH]{Obstruction}</li>

        <li>@Compendium[pf1spheres.magic-talents.1i015rTb4MmbJ8cm]{Painful
        Aegis}</li>

        <li>@Compendium[pf1spheres.magic-talents.Rvu7oq9lP9STILbg]{Peacebound}</li>

        <li>@Compendium[pf1spheres.magic-talents.62L7wEcfdvyJPeV7]{Plexing
        Aegis}</li>

        <li>@Compendium[pf1spheres.magic-talents.ZQmAVvQSAKrTFC4z]{Quantum
        Lock}</li>

        <li>@Compendium[pf1spheres.magic-talents.ApRKtvYdwflDemzU]{Ray
        Deflection}</li>

        <li>@Compendium[pf1spheres.magic-talents.T9iYXWODWulL828F]{Repel
        Chaos/Evil/Good/Law}</li>

        <li>@Compendium[pf1spheres.magic-talents.hgS3bEi8mJFWJWNj]{Resist
        Transformation}</li>

        <li>@Compendium[pf1spheres.magic-talents.ColYHA7i7u7jxG2i]{Resistance}</li>

        <li>@Compendium[pf1spheres.magic-talents.nF9ZLsWlkLkvCyPb]{Slippery}</li>

        <li>@Compendium[pf1spheres.magic-talents.0OvsNNr2jCVPo8Ee]{Spell
        Ward}</li>

        <li>@Compendium[pf1spheres.magic-talents.k0X45Pff19TnstH4]{Stabilize}</li>

        </ul><br /><b>Succor Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.X8g9G1iJ4lgrlCkJ]{Blast
        Shield}</li>

        <li>@Compendium[pf1spheres.magic-talents.RUhosFyYrT8SG1iy]{Bulwark
        (Protection)}</li>

        <li>@Compendium[pf1spheres.magic-talents.Da87Ct7ilfzGOzPg]{Healing
        Aegis}</li>

        <li>@Compendium[pf1spheres.magic-talents.W8c6Xl81nHzN8TkB]{Helping
        Hand}</li>

        <li>@Compendium[pf1spheres.magic-talents.uWWdLHzg3cgUDnLC]{Luck}</li>

        <li>@Compendium[pf1spheres.magic-talents.A4XKahIY5tK5EcyN]{Punishment}</li>

        <li>@Compendium[pf1spheres.magic-talents.VrXXYtJa9GMrhi77]{Reflection}</li>

        <li>@Compendium[pf1spheres.magic-talents.qmvFoQDQPdxdnpcB]{Vengeance}</li>

        </ul><br /><b>Advanced Protection Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.MacVLM8UeX6c9p1d]{Adaptation}</li>

        <li>@Compendium[pf1spheres.magic-talents.KeSBXa9aeU6WtIR7]{Antimagic
        Aura}</li>

        <li>@Compendium[pf1spheres.magic-talents.eFkaSR82f9QoZ0VI]{Complex
        Glyph}</li>

        <li>@Compendium[pf1spheres.magic-talents.nkbV2c5ihFxftwhI]{Mobile
        Ward}</li>

        <li>@Compendium[pf1spheres.magic-talents.endHzGCI9H3Ebgo4]{Permanent
        Ward}</li>

        <li>@Compendium[pf1spheres.magic-talents.cMTfweb6bOn8B8Ee]{Planar
        Refuge}</li>

        <li>@Compendium[pf1spheres.magic-talents.oR6i5qfRvFqOxYfV]{Preserve
        Integrity}</li>

        <li>@Compendium[pf1spheres.magic-talents.SDSPbFBkKaaY33BC]{Spell
        Selectivity}</li>

        <li>@Compendium[pf1spheres.magic-talents.PGSeNSMLs3XtgRZz]{Subtle}</li>

        <li>@Compendium[pf1spheres.magic-talents.YL2Pkw1pK3VGLayM]{True
        Protection}</li>

        <li>@Compendium[pf1spheres.magic-talents.1zOQ5uSmUInO75Fv]{Undying}</li>

        <li>@Compendium[pf1spheres.magic-talents.Lgp7eUpm6DDXJ4Ou]{Unplottable}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
