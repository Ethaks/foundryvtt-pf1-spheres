_id: ieAr2FubfesJeC6Q
_key: "!journal!ieAr2FubfesJeC6Q"
flags:
  pf1spheres:
    sphere: destruction
folder: null
name: Destruction
pages:
  - _id: gmVbVv1G4Yjvctpk
    _key: "!journal.pages!ieAr2FubfesJeC6Q.gmVbVv1G4Yjvctpk"
    flags: {}
    image: {}
    name: Destruction
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="modules/pf1spheres/assets/icons/spheres/destruction.webp"
        style="float:right;border:none;outline:none" />You can use destructive
        power.

        <h2 id="toc0"><span>Destructive Blast</span></h2>

        <p>As a standard action, you may deliver a burst of blunt magical force
        as a melee touch attack or a ranged touch attack within close range. A
        destructive blast is subject to spell resistance, and while it bypasses
        DR/magic, it does not automatically bypass other forms of damage
        reduction if it deals physical damage (bludgeoning, slashing, or
        piercing). A ranged destructive blast counts as a ray attack.</p>

        <p>A basic destructive blast deals 1d6 bludgeoning damage for every odd
        caster level.</p>

        <strong>Table: Destructive Blast Damage</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Level</th>

        <th>Damage</th>

        </tr>

        <tr>

        <td>1st</td>

        <td>1d6</td>

        </tr>

        <tr>

        <td>3rd</td>

        <td>2d6</td>

        </tr>

        <tr>

        <td>5th</td>

        <td>3d6</td>

        </tr>

        <tr>

        <td>7th</td>

        <td>4d6</td>

        </tr>

        <tr>

        <td>9th</td>

        <td>5d6</td>

        </tr>

        <tr>

        <td>11th</td>

        <td>6d6</td>

        </tr>

        <tr>

        <td>13th</td>

        <td>7d6</td>

        </tr>

        <tr>

        <td>15th</td>

        <td>8d6</td>

        </tr>

        <tr>

        <td>17th</td>

        <td>9d6</td>

        </tr>

        <tr>

        <td>19th</td>

        <td>10d6</td>

        </tr>

        <tr>

        <td>21st</td>

        <td>11d6</td>

        </tr>

        <tr>

        <td>23rd</td>

        <td>12d6</td>

        </tr>

        <tr>

        <td>25th</td>

        <td>13d6</td>

        </tr>

        </tbody></table>

        <p>You may spend 1 spell point when making a destructive blast to
        increase the damage dealt to one damage die per caster level (minimum
        2d6).</p>

        <h3 id="toc1"><span>Destruction Talent Types</span></h3>

        <p>When augmenting a destructive blast with Destruction talents, you may
        only apply 1 (blast type) talent and 1 (blast shape) talent to each
        individual destructive blast. If a blast type or blast shape grants a
        combat maneuver check, that maneuver ignores normal size
        limitations.</p>

        <h3 id="toc2"><span>Blast Type Groups</span></h3>

        Each (blast type) talent belongs to a blast type group with others of
        similar theme. For all purposes, the basic, unmodified destructive blast
        counts as its own blast type group.<br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Blast Type Group</th>

        <th>Blast Types</th>

        </tr>

        <tr>

        <td>Acid</td>

        <td>Acid Blast, Adhesive Blast, Alkali Blast</td>

        </tr>

        <tr>

        <td>Air</td>

        <td>Air Blast, Gale Blast, Hurricane Blast, Vacuum Blast</td>

        </tr>

        <tr>

        <td>Cold</td>

        <td>Drowning Blast, Frost Blast, Numbing Blast</td>

        </tr>

        <tr>

        <td>Crystal</td>

        <td>Crystal Blast, Living Crystal Blast, Razor Blast</td>

        </tr>

        <tr>

        <td>Electric</td>

        <td>Alternyating Current, Electric Blast, Shock Blast, Static Blast</td>

        </tr>

        <tr>

        <td>Fire</td>

        <td>Blistering Blast, Fire Blast, Searing Blast</td>

        </tr>

        <tr>

        <td>Force</td>

        <td>Force Blast, Invigorating Blast, Mana Siphon</td>

        </tr>

        <tr>

        <td>Holy</td>

        <td>Paradigm Blast, Smiting Blast</td>

        </tr>

        <tr>

        <td>Light</td>

        <td>Blinding Blast, Incandescent Blast, Radiant Blast</td>

        </tr>

        <tr>

        <td>Negative</td>

        <td>Gloom Blast, Nether Blast, Tenebrous Blast</td>

        </tr>

        <tr>

        <td>Sonic</td>

        <td>Reverberating Blast, Shattering Blast, Thunder Blast</td>

        </tr>

        <tr>

        <td>Stone</td>

        <td>Battering Blast, Shrapnel Blast, Stone Blast</td>

        </tr>

        </tbody></table>

        <hr /><br />

        <br /><b>Destruction Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.Pd2YKZ1ZvbxZotHa]{Admixture}</li>

        <li>@Compendium[pf1spheres.magic-talents.ICjQXKa4kBDmdVmU]{Cascade
        Failure}</li>

        <li>@Compendium[pf1spheres.magic-talents.oqUyUeTvI3Ug17Uf]{Clinging
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.7zuk9WJnmlO85v7I]{Crafted
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.vx5vMVp37422t1TQ]{Damage
        Control}</li>

        <li>@Compendium[pf1spheres.magic-talents.cqfxD5HFvvlq0tC3]{Demolition}</li>

        <li>@Compendium[pf1spheres.magic-talents.fUeBsxaARXaWbhJ3]{Energetic
        Response}</li>

        <li>@Compendium[pf1spheres.magic-talents.dm0dWaQKgc01dKnk]{Epicenter}</li>

        <li>@Compendium[pf1spheres.magic-talents.3Iwra3VaLSYdmDmW]{Extended
        Range}</li>

        <li>@Compendium[pf1spheres.magic-talents.FQgdD1mfv727k13P]{Focused
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.ct5bJd6ZtbEbIrmb]{Gather
        Energy}</li>

        <li>@Compendium[pf1spheres.magic-talents.xt7CBFOXYoIeJEKn]{Selective
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.2b2WiCL8HvbbBlSw]{Spirit
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.tRKSErB89Tfs4HRJ]{Wingbind}</li>

        </ul><br /><b>Blast Shape Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.2HxEFpcnIvk55Bg4]{Blast
        Salvo}</li>

        <li>@Compendium[pf1spheres.magic-talents.42lm0IZcjecuiU37]{Blast
        Trap}</li>

        <li>@Compendium[pf1spheres.magic-talents.FDIfbbxhPSOG2RuN]{Chain
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.eDWPYlFPFILHcqjx]{Energy
        Aura}</li>

        <li>@Compendium[pf1spheres.magic-talents.WoPizfIpkqVs3ovU]{Energy
        Bomb}</li>

        <li>@Compendium[pf1spheres.magic-talents.D5dAdVSOJHZbzNvT]{Energy
        Leap}</li>

        <li>@Compendium[pf1spheres.magic-talents.oKuXeDTW3NJ2lTqg]{Energy
        Satellite}</li>

        <li>@Compendium[pf1spheres.magic-talents.DEHrDuN09ngci7CV]{Energy
        Sphere}</li>

        <li>@Compendium[pf1spheres.magic-talents.DJ8WzqrFIIEzIjz4]{Energy
        Strike}</li>

        <li>@Compendium[pf1spheres.magic-talents.Qr40quMHzc655bs5]{Energy
        Tether}</li>

        <li>@Compendium[pf1spheres.magic-talents.HpsFYhrwWWpDMkWw]{Energy
        Wall}</li>

        <li>@Compendium[pf1spheres.magic-talents.s1xDrU5T6IfFgx4M]{Explosive
        Orb}</li>

        <li>@Compendium[pf1spheres.magic-talents.08aZa7uMgUvtUKhY]{Guided
        Strike}</li>

        <li>@Compendium[pf1spheres.magic-talents.vTX3lo4X4XrGTIYC]{Knight’s
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.nPVzgrUri5IrHyJI]{Mutable
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.xNJgDZbAtqB0kC2w]{Rebuff}</li>

        <li>@Compendium[pf1spheres.magic-talents.9eRUIN6aWUT1aPcw]{Retributive
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.L3mMiXKsHFBUrmTR]{Sculpt
        Blast}</li>

        </ul><br /><b>Blast Type Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.ROwc4BLsV3ECGzh1]{Acid
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.4jLhaTkDY2pNOxyx]{Adhesive
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.PjueM5z4eylwlWbK]{Air
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.EU0TOwGQcGW460XI]{Alkali
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.R8ulOOz5y1mzxuTh]{Alternyating
        Current}</li>

        <li>@Compendium[pf1spheres.magic-talents.V1oKK9NVl9HrCnAk]{Battering
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.M1Mo6z5YUYRLZmE2]{Blinding
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.qPtxZFCw8t4ETyby]{Blistering
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.zncomDzj7bhtnf6j]{Crystal
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.lwGPX1FewncIiKWa]{Drowning
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.vT54QIdZuL6aG0h8]{Electric
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.EXbxrzL6nyZrdIwl]{Fire
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.k62AgH9T71dMcvmD]{Force
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.J5TWF6vBFRBPq51J]{Frost
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.omH4t7ib2hcdhpAW]{Gale
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.356cEdpzdK2iqOrj]{Gloom
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.cF23SJLG2j55bsGX]{Gore
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.qhHR5KjdVzcm4Z6v]{Hurricane
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.xT1uyqjPcwgyOXat]{Incandescent
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.CPBQ1Z6Odiqe4vFk]{Invigorating
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.MjuYm4bfTqfyyyLw]{Living
        Crystal Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.5VUKW3YwYRAUsNNL]{Mana
        Siphon}</li>

        <li>@Compendium[pf1spheres.magic-talents.YJAfnOhE8T3WyvYX]{Nether
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.yc4sxOcV7zbL3fIO]{Numbing
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.kumPYCd7k4kkp7DE]{Paradigm
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.FN5VjtFukDGS9Bf1]{Radiant
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.mjndjBxASW6wsbRg]{Razor
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.UqcOKqXaw0BRtuBG]{Reverberating
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.ixlbHJv5Z7sXKQS6]{Searing
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.QFoJbXttyFvnGAxX]{Shattering
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.E0CuBNuyt9pfE4gc]{Shock
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.jJX1kj9g7rK1BWIz]{Shrapnel
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.UdD3Z0aE6nNH0hpZ]{Smiting
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.6N8WvJhawXgiNpB9]{Static
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.eWHB04msYKy3QBGU]{Stone
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.r4Eqf7kmBEd3wmkC]{Tenebrous
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.p7nbxE6ZG9UECX8Z]{Thunder
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.k9xvDB4DSTqGHnKa]{Vacuum
        Blast}</li>

        </ul><br /><b>Advanced Destruction Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.qEn4Nz2JQWCKGZDz]{Blast
        Array}</li>

        <li>@Compendium[pf1spheres.magic-talents.QgCdGP1CmzcW1SNa]{Calamity}</li>

        <li>@Compendium[pf1spheres.magic-talents.V5Adw6QfttJjwdTz]{Crystal
        Cocoon}</li>

        <li>@Compendium[pf1spheres.magic-talents.gKYG4ZvovMPmcYX8]{Disintegrate}</li>

        <li>@Compendium[pf1spheres.magic-talents.0t4Rg2YlnAkNELZH]{Energy
        Cloud}</li>

        <li>@Compendium[pf1spheres.magic-talents.UuUCEEaqGP0DcyDO]{Erratic
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.5aO1wb4r6TKzggX8]{Extreme
        Range}</li>

        <li>@Compendium[pf1spheres.magic-talents.IlwYHPrYJgFmADBw]{Grandmaster}</li>

        <li>@Compendium[pf1spheres.magic-talents.2E0klSIoRWv6DLVT]{Greater
        Admixture}</li>

        <li>@Compendium[pf1spheres.magic-talents.m2phaMPxJ50ZJe9E]{Penetrating
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.6mXYv8ejuWwEScHy]{Radiation
        Blast}</li>

        <li>@Compendium[pf1spheres.magic-talents.cPb3QnRi0MicKO8m]{Split
        Blast}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
