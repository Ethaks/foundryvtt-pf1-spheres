_id: FEuVJjK7NYQRtnUT
_key: "!journal!FEuVJjK7NYQRtnUT"
flags:
  pf1spheres:
    sphere: mana
folder: null
name: Mana
pages:
  - _id: p3jS6YjY1Hsv43yb
    _key: "!journal.pages!FEuVJjK7NYQRtnUT.p3jS6YjY1Hsv43yb"
    flags: {}
    image: {}
    name: Mana
    ownership:
      default: -1
    sort: 0
    src: null
    system: {}
    text:
      content: >-
        <img src="icons/svg/item-bag.svg"
        style="float:right;border:none;outline:none" /><strong>The Flow of
        Magic:</strong> Some talents or abilities in the Mana sphere, as well as
        some other spheres, grant temporary spell points. Only temporary spell
        points from different sources stack together. Temporary spell points
        from any source may never be used to cast Mana sphere effects. Temporary
        spell points granted by the Mana sphere only last 1 round per caster
        level if not specified otherwise. Temporary spell points must always be
        spent first, and are always lost first when a creature loses spell
        points from any effect. Finally, any effect with a non-instantaneous
        duration cast with temporary spell points expires when the the temporary
        spell points would expire, even if their duration would otherwise be
        longer. If an effect uses temporary spell points with different
        remaining durations, use the shortest remaining duration to determine
        the effects cast with them.

        <p>When attempting a magic skill check against a creature that does not
        have a magic skill defense, treat their magic skill defense as if it
        were 11.</p>

        <p><strong>Sphere Restrictions:</strong> Unless otherwise specified,
        animal companions, cohorts, Conjuration sphere companions, drake
        companions, eidolons, familiars, followers and other similar companions
        or pets cannot gain this sphere or talents from this sphere.</p>

        <p><strong>Special: Destruction Sphere</strong> If you possess the Mana
        Siphon (blast type) talent, you may create manabonds on any creature you
        drain spell points from while using it, treating it as if it was an
        expunge. Additionally, temporary spell points gained from Mana Siphon
        benefit from the Retained Imbuement talent. Mana Siphon always drains
        real spell points, not temporary ones.</p>

        <hr />

        <h2 id="toc0"><span>Expunge</span></h2>

        <p>As a standard action, you may expunge the energy from a creature
        within close range, applying the effects of a single (expunge) talent
        that you possess upon the creature.</p>

        <p>When you gain the Mana sphere, you gain the following (expunge)
        talent:</p>

        <h4 id="toc1"><span>Spellburn</span></h4>

        <p>You may spend a spell point and attempt a magic skill check against
        your target. If successful, you burn a number of spell points from the
        target’s spell pool, removing it from their spell pool as if they had
        used the spell point themselves (although this does not trigger effects
        that rely on spell points being spent, such as drawbacks from the
        target’s casting tradition, or the triggering of wild magic). The amount
        of spell points burned starts 1d3 and increases at 5th level and every
        four caster levels thereafter (up to a maximum of 3d6 at 25th caster
        level), as listed under Table: Burn Dice.</p>

        <strong>Table: Burn Dice</strong><br />

        <table class="wiki-content-table">

        <tbody><tr>

        <th>Caster Level</th>

        <th>Burn Size</th>

        </tr>

        <tr>

        <td>1st</td>

        <td>1d3</td>

        </tr>

        <tr>

        <td>5th</td>

        <td>1d4</td>

        </tr>

        <tr>

        <td>9th</td>

        <td>1d6</td>

        </tr>

        <tr>

        <td>13th</td>

        <td>2d4</td>

        </tr>

        <tr>

        <td>17th</td>

        <td>2d6</td>

        </tr>

        <tr>

        <td>21st</td>

        <td>3d4</td>

        </tr>

        <tr>

        <td>25th</td>

        <td>3d6</td>

        </tr>

        </tbody></table>

        <p><strong>Note:</strong> The expunge ability is created with the
        intention of being used upon other spellcasters that utilize the Spheres
        of Power system of magic, but can be made to work with other systems of
        magic as well. When used against a spellcaster that utilizes the core
        system of magic in the Pathfinder Roleplaying Game, a GM may allow it to
        burn spell slots or spells prepared. To calculate which spells are
        affected, total the number of spell points burned. The caster suffering
        from the Spellburn talent may then choose an amount of spell slots or
        prepared spells to lose. They must choose an amount of spells that
        amount to the points cost, following this list to determine the value of
        each spell level in spell points:</p>

        <p><strong>Level 1-2:</strong> 1 spell point, <strong>Level
        3-5:</strong> 2 spell points, <strong>Level 6-8:</strong> 3 spell
        points, <strong>Level 9:</strong> 4 spell points.</p>

        <p>If there is an amount of spell points remaining that the spellcaster
        cannot sacrifice spells of a level to make up for without going above
        the amount (for example, only losing 1 spell point but not having any
        1st or 2nd level spell slots/prepared spells left), they may ignore the
        remainder without sacrificing additional spell slots. When used on
        creatures with psionic manifesting (Psionics Unleashed by Dreamscarred
        Press), the creature loses a number of power points equal to the number
        of spell points that would have been lost multiplied by 1 + its Hit
        Dice/4.</p>

        <h2 id="toc2"><span>Manipulation</span></h2>

        <p>You learn to bend the rules of magic to your advantage. As a standard
        action, you may use any (manipulation) that you possess, up to a range
        of close. You must remain within this range when maintaining a
        (manipulation) through concentration. When you gain the Mana sphere, you
        gain the following (manipulation):</p>

        <h4 id="toc3"><span>Shuffle</span></h4>

        <p>You may manipulate an area within range, altering the rules of magic
        within a radius of up to 10 feet + 5 feet per 10 caster levels. You may
        select a single modification to make to the manipulated area at the time
        of casting, listed below. This area remains for as long as you
        concentrate, to a maximum of 1 round per caster level, but you may
        always spend a spell point to allow the effect to remain without
        concentration for its maximum duration. All spells cast in or into the
        affected area, or that pass through the area (such as a ray) are subject
        to this ability’s effects.</p>

        <p>Only one shuffle can be in effect in an area at a time, with any
        attempt to overlap areas requiring a magic skill check against the MSD
        of the initial shuffle effect. If you succeed, the initial shuffle
        effect is dispelled, but on a failure you fail to cast the shuffle
        effect.</p>

        <p>You may modify the rules of magic in the following ways:</p>

        <p><strong>Elemental Rewiring:</strong> Select a single energy type from
        the following list: acid, cold, electricity, or fire. You convert all
        energy damage from magical and supernatural effects that are affected by
        your shuffle into the selected type, but only if they are energy types
        that are on the prior list of energy types. At 7th caster level, you add
        sonic to this list. At 12th caster level, you add force to this list. At
        17th caster level, you add negative and positive energy to this
        list.</p>

        <p><strong>Protective Rewiring:</strong> Select a single saving throw
        between Fortitude, Reflex and Will. Whenever a creature within the area
        of your shuffle must attempt a saving throw against a magical or
        supernatural effect, they must instead use the modifier of the saving
        throw you chose.</p>

        <p>The saving throw is still treated as the original type required by
        the magical or supernatural effect (i.e., evasion still works when
        switching from Reflex to Fortitude), and any conditional modifiers that
        apply to the original type still apply, but not from the new type. The
        caster of a sphere effect changed by this ability may attempt a magic
        skill check against your Mana sphere DC to stop creatures from utilizing
        this effect against that specific sphere effect.</p>

        <h2 id="toc4"><span>Manabond</span></h2>

        <p>As a swift action made as part of successfully using an expunge or
        (manipulation) that targets a creature other than yourself, or as a move
        action made by spending a spell point to touch a creature other than
        yourself, you can create a manabond between yourself and the targeted
        creature for as long as you concentrate, to a maximum of 1 minute per
        caster level. Concentration on a manabond only requires a swift action,
        with the action required being unable to be lowered by any means other
        than the Magical Conduit talent. When creating a manabond, you may add a
        single (manabond) talent that you possess to the effect.</p>

        <p>A manabond is a direct, invisible and intangible line of energy that
        connects you and your target, and remains active up to close range,
        beyond which the manabond breaks, and the effect ends. As long as you
        have a manabond with a creature, you may always pinpoint their exact
        location (although this does not negate any concealment they may
        have).</p>

        <p>When creating a manabond, you are considered the host of the
        manabond, with the targeted creature considered the recipient. A
        creature can only be the host of a single manabond, and cannot be the
        recipient of multiple manabonds at the same time. If a second manabond
        would be cast on a creature, the caster must attempt a magic skill check
        against the MSD of the currently active manabond on the target. If the
        caster succeeds, the other manabond is suppressed, causing it to remain
        active and ongoing as long as it is maintained, but having no effect on
        the recipient until the suppressing manabond has ended.</p>

        <p>If the caster fails, their manabond becomes suppressed, with the
        original manabond remaining active. Even when suppressed, a host may
        still pinpoint the location of the recipient.</p>

        <p>When you gain the Mana sphere, you gain the following (manabond)
        talent:</p>

        <h4 id="toc5"><span>Mystical Bond</span></h4>

        <p>You can assist the recipient of your manabond in their spherecasting,
        or hamper their efforts. When creating this manabond, you can choose to
        apply either a penalty or a competence bonus to their caster level, but
        only for a single sphere, decided at the time of casting. This bonus or
        penalty begins at 1, and increases by 1 every eight caster levels you
        possess. The penalty cannot reduce a creature’s caster level below
        1.</p>

        <h3 id="toc6"><span>Mana Talent Types</span></h3>

        <p>Some talents are marked (expunge). These grant you additional ways to
        expunge energy.</p>

        <p>Some talents are marked (manipulation). These grant you additional
        ways of manipulating energy.</p>

        <p>Some talents are marked (manabond). These grant you additional ways
        of using your manabonds.</p>

        <hr /><br />

        <br /><b>Mana Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.fN5Al10HBDkH9z0T]{Defensive
        Bond}</li>

        <li>@Compendium[pf1spheres.magic-talents.qbgJTXgwkIUXNyi2]{Draining
        Spellburn}</li>

        <li>@Compendium[pf1spheres.magic-talents.F64YsxVumlTzRSYm]{Explosive
        Expunge}</li>

        <li>@Compendium[pf1spheres.magic-talents.gj6jWWYzUYdsrggF]{Hardened
        Bond}</li>

        <li>@Compendium[pf1spheres.magic-talents.r6Of3wyGLOb5dRj7]{Magical
        Conduit}</li>

        <li>@Compendium[pf1spheres.magic-talents.3MfrGSw0wq2240HE]{Manasurge
        Strike}</li>

        <li>@Compendium[pf1spheres.magic-talents.uBu26mZBCRc9O9QW]{Manavore}</li>

        <li>@Compendium[pf1spheres.magic-talents.qbJCNCuJucXtw0y2]{Overpowering
        Expunge}</li>

        <li>@Compendium[pf1spheres.magic-talents.5ivpTOZwqY7ISNH1]{Ranged
        Mana}</li>

        <li>@Compendium[pf1spheres.magic-talents.0tyraAHOmSXFrnHr]{Retained
        Imbuement}</li>

        <li>@Compendium[pf1spheres.magic-talents.1xumIODIaET7BPRX]{Reactive
        Shuffle}</li>

        <li>@Compendium[pf1spheres.magic-talents.HG0WstNM0IGCavnq]{Sudden
        Empowerment/Weakening}</li>

        </ul><br /><b>Expunge Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.6nrgzybHN6bJJcQv]{Disorient}</li>

        <li>@Compendium[pf1spheres.magic-talents.MKIMTIxOR5J6GLFM]{Ignition}</li>

        <li>@Compendium[pf1spheres.magic-talents.IVEPx55SEJmOyQ9p]{Mark Of
        Incompetency}</li>

        <li>@Compendium[pf1spheres.magic-talents.GOBpeRviOL4dCNiH]{Mark Of
        Instability}</li>

        <li>@Compendium[pf1spheres.magic-talents.bFlwbwMW1uA1Sj2n]{Mark Of
        Lifeburn}</li>

        </ul><br /><b>Manabond Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.wjvuNTkJSWmqg8CI]{Control
        Resistance}</li>

        <li>@Compendium[pf1spheres.magic-talents.QqAcF29OS9xUUWPb]{Flow}</li>

        <li>@Compendium[pf1spheres.magic-talents.a7bI0py5cBhvmu7I]{Magical
        Misdirection}</li>

        <li>@Compendium[pf1spheres.magic-talents.ouE1ghbGq3jOZyLB]{Manabond
        Channeling}</li>

        <li>@Compendium[pf1spheres.magic-talents.NvIWv7g9SflCffBy]{Soulmate}</li>

        </ul><br /><b>Manipulation Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.hqGb2Ke49we6CFRM]{Bulwark
        (Mana)}</li>

        <li>@Compendium[pf1spheres.magic-talents.F8W2mt7lziWdCr9p]{Essentialize}</li>

        <li>@Compendium[pf1spheres.magic-talents.WiR7hsNy8cgfFwy5]{Gift Of
        Knowledge}</li>

        <li>@Compendium[pf1spheres.magic-talents.tdybJ4a7QF0BdlVT]{Relinquish
        Magic}</li>

        <li>@Compendium[pf1spheres.magic-talents.KrgIrANH4z9EASYG]{Transfer}</li>

        </ul><br /><b>Advanced Mana Talents</b><br /><ul>

        <li>@Compendium[pf1spheres.magic-talents.dXHDrTNzldBEQdY4]{Cruel
        King}</li>

        <li>@Compendium[pf1spheres.magic-talents.KmCwLxM36dRJOdUH]{Curse Of
        Mana}</li>

        <li>@Compendium[pf1spheres.magic-talents.GFefMiwaG2EFS0uW]{Eternal
        Shackles}</li>

        <li>@Compendium[pf1spheres.magic-talents.4kU1gLuYCcxFDnlF]{Harness
        Ambient Knowledge}</li>

        <li>@Compendium[pf1spheres.magic-talents.N6ooFj00dVwhqyAC]{Infinite
        Bond}</li>

        <li>@Compendium[pf1spheres.magic-talents.0TqcNszNuPy9GVDa]{Knowledge
        Drain}</li>

        <li>@Compendium[pf1spheres.magic-talents.ZXsiSUdlDs4kVJK2]{Manathief}</li>

        <li>@Compendium[pf1spheres.magic-talents.RhkcQqWHeNkO6OWW]{Permanent
        Bond}</li>

        <li>@Compendium[pf1spheres.magic-talents.2ZZMfLoHvfDfay7l]{Ties That
        Bind}</li>

        <li>@Compendium[pf1spheres.magic-talents.OCsVjpr56nN7TJyr]{Unbind
        Spell}</li>

        <li>@Compendium[pf1spheres.magic-talents.34F7nr8wBiD8mTfi]{Vassalize}</li>

        <li>@Compendium[pf1spheres.magic-talents.LHVVmYzhIyM5G4Bf]{What’s Yours
        Is Ours}</li></ul>
      format: 1
    title:
      level: 1
      show: false
    type: text
    video:
      controls: true
      volume: 0.5
sort: 0
